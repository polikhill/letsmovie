//
//  filmDetails.swift
//  LetsMovie
//
//  Created by Polina on 10/31/17.
//  Copyright © 2017 Polina. All rights reserved.
//

import UIKit

public class filmDetails: NSObject {
    static let filmNames = ["Geostorm", "IT", "Blade Runner 2049"]
    static let posterImage: [UIImage] = [
        UIImage(named: "geostorm.jpeg")!,
        UIImage(named: "it.jpeg")!,
        UIImage(named: "bladeRunner.jpeg")!
    ]
    static let releaseDate  = "2017"
    static let genre: [String] = ["action, triller, fantasy", "horror", "action, triller, fantasy"]
    static let country = "United States"
    static let actors: [String] = [
        """
Abbi Cornish, Katrin Vinnyk,
Jerard Buttler, Jim Stardgess
""",
"Jaeden Lieberher, Bill Skarsgård",
"""
Ryan Gosling, Harrison Ford,
Ana de Armas, Sylvia Hoeks
"""]
    static let director = ["Dean Davline", "Andy Muchietti", "Denis Villeneuve"]
    static let duration = [109, 108, 144]
    static let language = ["English", "English", "English"]
    static let filmDescription = [
        """
When the network of satellites designed to control
the global climate starts to attack Earth,
it's a race against the clock to uncover
the real threat before a worldwide Geostorm
wipes out everything and everyone.
""",

        """
A group of bullied kids band together when a
shapeshifting demon, taking the appearance
of a clown, begins hunting children.
""",

        """
A young blade runner's discovery of a
long-buried secret leads him to track
down former blade runner Rick Deckard,
who's been missing for thirty years.
"""]
    
    let textColor = UIColor.init(red: 244, green: 67, blue: 54, alpha: 1)
    let myAttribute =
        [ NSAttributedStringKey.foregroundColor: UIColor.init(red: 244, green: 67, blue: 54, alpha: 1) ]
	
	static var releaseDateAttributedString: NSAttributedString {
		let releaseDate = NSAttributedString(string: "Release date: ", attributes: [ NSAttributedStringKey.foregroundColor: UIColor.init(red: 244, green: 67, blue: 54, alpha: 1)])
		return releaseDate
	}
	
//    let releaseDateAttr = NSAttributedString(string: "Release date: ", attributes: [ NSAttributedStringKey.foregroundColor: UIColor.init(red: 211, green: 47, blue: 47, alpha: 1)])
	
//    let releaseDate = NSAttributedString(string: "Release date: ", attributes: myAttribute)
//    let country = NSAttributedString(string: "Country: ", attributes: myAttribute)
//    let genre = NSAttributedString(string: "Genre: ", attributes: myAttribute)
//    let starring = NSAttributedString(string: "Starring: ", attributes: myAttribute)
//    let directedBy = NSAttributedString(string: "Directed by: ", attributes: myAttribute)
//    let duration = NSAttributedString(string: "Duration: ", attributes: myAttribute)
//    let language = NSAttributedString(string: "Language: ", attributes: myAttribute)
}
