//
//  FilmDetailsCustomTableViewCell.swift
//  LetsMovie
//
//  Created by Polina on 11/6/17.
//  Copyright © 2017 Polina. All rights reserved.
//

import UIKit

class FilmDetailsCustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var poster: UIImageView!
    
    @IBOutlet weak var actors: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var filmName: UILabel!
    @IBOutlet weak var director: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var filmDescription: UILabel!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
}

