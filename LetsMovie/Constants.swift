//
//  Constants.swift
//  LetsMovie
//
//  Created by Polina on 11/8/17.
//  Copyright © 2017 Polina. All rights reserved.
//

import Foundation
import UIKit

let filmNames = ["Geostorm", "IT", "Blade Runner 2049"]
let posterImage: [UIImage] = [
    UIImage(named: "geostorm.jpeg")!,
    UIImage(named: "it.jpeg")!,
    UIImage(named: "bladeRunner.jpeg")!
]
let releaseDate  = "2017"
let genre: [String] = ["action, triller, fantasy", "horror", "action, triller, fantasy"]
let country = "United States"
let actors: [String] = [
    """
Abbi Cornish, Katrin Vinnyk,
Jerard Buttler, Jim Stardgess
""",
    "Jaeden Lieberher, Bill Skarsgård",
    """
Ryan Gosling, Harrison Ford,
Ana de Armas, Sylvia Hoeks
"""]
let director = ["Dean Davline", "Andy Muchietti", "Denis Villeneuve"]
let duration = [109, 108, 144]
let language = ["English", "English", "English"]
let filmDescription = [
    """
When the network of satellites designed to control
the global climate starts to attack Earth,
it's a race against the clock to uncover
the real threat before a worldwide Geostorm
wipes out everything and everyone.
""",
    
    """
A group of bullied kids band together when a
shapeshifting demon, taking the appearance
of a clown, begins hunting children.
""",
    
    """
A young blade runner's discovery of a
long-buried secret leads him to track
down former blade runner Rick Deckard,
who's been missing for thirty years.
"""]
