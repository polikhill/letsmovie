//
//  ViewController.swift
//  LetsMovie
//
//  Created by Polina on 10/25/17.
//  Copyright © 2017 Polina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.dataSource = self
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filmNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:FilmDetailsCustomTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "filmDetailsCell") as! FilmDetailsCustomTableViewCell
        
        
        cell.poster.image = posterImage[indexPath.row]
        cell.filmName.text = filmNames[indexPath.row]
        let prefix = NSAttributedString(string: "Release Date: ", attributes: [NSAttributedStringKey.foregroundColor : UIColor.black])
        let suffix = NSAttributedString(string: releaseDate, attributes: [NSAttributedStringKey.foregroundColor : UIColor.orange])
        let result = prefix + suffix
        cell.releaseDate.attributedText = result
        cell.genre.text = genre[indexPath.row]
        cell.country.text = country
        cell.actors.text = actors[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedFilm = indexPath.row
        self.performSegue(withIdentifier: "showDetail", sender: selectedFilm)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let destinationVC = segue.destination as? filmDetailsVC {
                destinationVC.selectedFilm = sender as! Int
            }
        }
    }
}




