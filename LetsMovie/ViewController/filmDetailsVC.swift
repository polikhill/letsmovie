//
//  filmDetailsVC.swift
//  LetsMovie
//
//  Created by Polina on 11/4/17.
//  Copyright © 2017 Polina. All rights reserved.
//

import UIKit

class filmDetailsVC: UIViewController, UITableViewDataSource {
    
    var datePickerHidden = false
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedFilm = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.dataSource = self
    }
}

extension filmDetailsVC {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filmNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let videoCell:FilmDetailsCustomTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "videoCell") as! FilmDetailsCustomTableViewCell
            
            return videoCell
        }
            
        else if indexPath.row == 1 {
            let detailsCell:FilmDetailsCustomTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "detailsCell") as! FilmDetailsCustomTableViewCell
            
            detailsCell.poster.image =  posterImage[selectedFilm]
            detailsCell.releaseDate.text = releaseDate + releaseDate
            detailsCell.genre.text = genre[selectedFilm]
            detailsCell.country.text = country
            detailsCell.actors.text = actors[selectedFilm]
            detailsCell.director.text = director[selectedFilm]
            detailsCell.duration.text = String(duration[selectedFilm])
            detailsCell.language.text = language[selectedFilm]
            detailsCell.filmDescription.text = filmDescription[selectedFilm]
            
            return detailsCell
        }
            
        else if indexPath.row == 2 {
            let datePickerCell:FilmDetailsCustomTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "datePickerCell") as! FilmDetailsCustomTableViewCell
            
            return datePickerCell
        }
            
        else {
            let timePickerCell:FilmDetailsCustomTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "timePickerCell") as! FilmDetailsCustomTableViewCell
            
            return timePickerCell
        }
    }
    
    
}

